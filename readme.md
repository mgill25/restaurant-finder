Restaurant Listings
-------------------

A small Django application.

    - Show listing of new restaurants around town.
    - Visit a place
    - Add reviews. Comment on reviews.
    - Geo coordinates for restaurant address.
    - Don't display places which have been "thumbed down" by a user.


Setup
-----
    
    - Setup a virtualenv
    - `pip install -r requirements.txt` to install dependencies
    - `python manage.py collectstatic`
    - `python manage.py migrate`
    - `python manage.py runserver`

