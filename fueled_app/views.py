# Django imports
from django.shortcuts import render, redirect

# Internal application imports
from .models import (
    Place,
    User,
    Review
)
from .forms import (
    LoginForm,
    SignupForm,
    ReviewForm,
    CommentForm,
    PlaceForm,
    UserProfileForm
)

# Third party library imports
from geopy.geocoders import GoogleV3


def index(request):
    '''Landing page'''
    return render(request, 'fueled_app/index.html')


def login_user(request):
    if request.method == 'POST':
        # Form was submitted!
        user = User.objects.get(username=request.POST['username'])
        form = LoginForm(request.POST, instance=user)
        if form.is_valid():
            user = User.objects.get(username=form.cleaned_data['username'])
            if user:
                validate = user.check_password(form.cleaned_data['password'])
                if validate:
                    # skipping using django's authentication/login backend here
                    # as that requires setting up AUTH_USER_MODEL at the beginning
                    # of project. We'll just store username in session and query using that.
                    request.session['username'] = form.cleaned_data['username']
                    return redirect('dashboard')
                else:
                    print('Bad password')
            else:
                print('User not found!')
            return redirect('login')
        else:
            print('Invalid form!')
            return redirect('login')
    else:
        form = LoginForm()
        return render(request, 'fueled_app/login.html', {'form': form})


def logout_user(request):
    request.session.clear()
    return redirect('index')


def signup(request):
    if request.method == 'POST':
        # Form was submitted!
        form = SignupForm(request.POST)
        if form.is_valid():
            return redirect('dashboard')
        else:
            return redirect('signup')
    else:
        form = SignupForm()
        return render(request, 'fueled_app/signup.html', {'form': form})


def single_place(request, place):
    '''Individual Restaurant listing page'''
    if request.method == 'GET':
        # Return the place!
        item = Place.objects.get(name=place)
        if item:
            request.session['place'] = item.name
            username = request.session['username']
            user = User.objects.get(username=username)
            reviewform = ReviewForm()
            commentform = CommentForm()
            ctx = {
                'item': item,
                'reviewform': reviewform,
                'commentform': commentform,
                'past_visitors': [
                    u.first_name for u in item.past_visitors \
                        if u.username != username
                ],
                'visited': user.has_visited(item)
            }
            return render(request,
                          'fueled_app/single_place.html', ctx)
        return redirect('dashboard')


def dashboard(request):
    '''Dashboard after login'''
    # Displays all the new places I haven't visited yet!
    username = request.session['username']
    user = User.objects.get(username=username)
    new_places = user.new_places()
    return render(request, 'fueled_app/dashboard.html',
                  {'new_places': new_places, 'user': user})


def add_new_place(request):
    '''Add a new place'''

    if request.method == 'POST':
        username = request.session['username']
        user = User.objects.get(username=username)
        if not user.can_add_places:
            print('Error: You do not have permission to add new places!')
            return redirect('dashboard')
        form = PlaceForm(request.POST)
        if form.is_valid():
            # calculate the lat and lon!
            name = form.cleaned_data['name']
            address = form.cleaned_data['address']
            city = form.cleaned_data['city']
            zipcode = form.cleaned_data['zipcode']

            geolocator = GoogleV3()
            latlong = geolocator.geocode("{} {} {}".format(address, city, zipcode))
            if not latlong:
                print('Error: Unable to locate coordinates!')
                lat, lon = 0.00000, 0.00000                    # zero results on this
            else:
                lat = latlong.latitude
                lon = latlong.longitude

            # Now add in the place!
            user.add_new_place(
                name=name,
                address=address,
                city=city,
                zipcode=zipcode,
                lat=lat,
                lon=lon
            )
            return redirect('dashboard')
        else:
            print('Invalid form data!')
            return redirect('dashboard')
    else:
        form = PlaceForm()
        ctx = {
            'form': form
        }
        return render(request, 'fueled_app/add_new_place.html', ctx)


def add_review(request):
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review_text = form.cleaned_data['text']
            user = User.objects.get(username=request.session['user'])
            place = Place.objects.get(name=request.session['place'])
            place.add_review(user, review_text)
            return redirect('single_place', place.name)
    return render(request, 'fueled_app/single_place.html')


def add_comment(request, review_pk):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_text = form.cleaned_data['text']
            review = Review.objects.get(pk=review_pk)
            place = Place.objects.get(name=request.session['place'])
            review.add_comment(comment_text)
            return redirect('single_place', place.name)
    return render(request, 'fueled_app/single_place.html')


def profile(request):
    '''User profile'''
    if request.method == 'POST':
        username = request.session['username']
        user = User.objects.get(username=username)
        profile = user.profile
        form = UserProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return redirect('profile')
    return render(request, 'fueled_app/profile.html', {'form': UserProfileForm()})
