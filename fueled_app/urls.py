__author__ = 'manish'

from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^places/(?P<place>\w+)/$', views.single_place, name='single_place'),
    url(r'^profile/$', views.dashboard, name='profile'),
    url(r'^add_review/$', views.add_review, name='add_review'),
    url(r'^add_new_place/$', views.add_new_place, name='add_new_place'),
    url(r'^add_comment/(?P<review_pk>\d+)/$', views.add_comment, name='add_comment'),
]
