# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('lon', models.DecimalField(max_digits=9, decimal_places=6)),
                ('lat', models.DecimalField(max_digits=9, decimal_places=6)),
                ('address', models.TextField()),
                ('city', models.TextField()),
                ('zipcode', models.TextField()),
                ('creation_time', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('creation_time', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('username', models.CharField(unique=True, max_length=254)),
                ('first_name', models.CharField(unique=True, max_length=30)),
                ('last_name', models.CharField(unique=True, max_length=30)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('can_add_places', models.BooleanField(default=False)),
                ('creation_time', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='review',
            name='created_by',
            field=models.ForeignKey(to='fueled_app.User'),
        ),
        migrations.AddField(
            model_name='review',
            name='place',
            field=models.ForeignKey(to='fueled_app.Place'),
        ),
        migrations.AddField(
            model_name='place',
            name='patrons',
            field=models.ManyToManyField(to='fueled_app.User'),
        ),
    ]
