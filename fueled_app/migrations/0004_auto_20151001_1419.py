# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fueled_app', '0003_auto_20151001_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='place',
            name='address',
            field=models.TextField(unique=True),
        ),
        migrations.AlterField(
            model_name='place',
            name='name',
            field=models.TextField(unique=True),
        ),
    ]
