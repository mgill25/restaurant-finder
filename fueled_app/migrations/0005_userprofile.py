# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fueled_app', '0004_auto_20151001_1419'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gender', models.CharField(max_length=1, choices=[(b'M', b'Male'), (b'F', b'Female'), (b'O', b'Other')])),
                ('address', models.TextField(unique=True)),
                ('city', models.TextField()),
                ('zipcode', models.TextField()),
                ('avatar', models.ImageField(upload_to=b'avatars/', blank=True)),
                ('user', models.OneToOneField(to='fueled_app.User')),
            ],
        ),
    ]
