# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fueled_app', '0002_auto_20150929_1321'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('creation_time', models.DateTimeField(default=django.utils.timezone.now)),
                ('review', models.ForeignKey(to='fueled_app.Review')),
            ],
        ),
        migrations.AddField(
            model_name='place',
            name='blacklisted',
            field=models.BooleanField(default=False),
        ),
    ]
