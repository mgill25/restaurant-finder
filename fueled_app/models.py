from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager
)

from django.utils import timezone


class PermissionError(Exception):
    pass


class CustomUserManager(BaseUserManager):
    def create_user(self, username, email, first_name, last_name, password):
        if not email:
            raise ValueError('Must have an email address')
        if not password:
            raise ValueError('Must have a password')

        user = self.model(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name
        )
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser):
    username = models.CharField(max_length=254, unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(blank=True)

    can_add_places = models.BooleanField(default=False)
    creation_time = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['username', 'email', 'password']

    objects = CustomUserManager()

    def visit(self, place):
        place.patrons.add(self)
        place.save()

    def has_visited(self, place):
        return place in self.place_set.all()

    def new_places(self):
        """Non-blacklisted places that I haven't visited yet"""
        return Place.objects.filter(blacklisted=False).exclude(patrons__in=[self])

    def add_new_place(self, name, lon, lat, address, city, zipcode):
        if not self.can_add_places:
            raise PermissionError("You cannot add new places.")
        Place.objects.create(
            name=name,
            lon=lon,
            lat=lat,
            address=address,
            city=city,
            zipcode=zipcode
        )

    def add_profile_data(self, gender, address, city, zipcode):
        user_profile, created = UserProfile.objects.update_or_create(
            gender=gender,
            address=address,
            city=city,
            zipcode=zipcode,
            user=self
        )
        return user_profile

    @property
    def profile(self):
        return self.userprofile

    def __unicode__(self):
        return self.username


class Place(models.Model):
    """
    A generic place - can be a restaurant.
    """
    name = models.TextField(unique=True)

    # address info
    lon = models.DecimalField(max_digits=9, decimal_places=6)
    lat = models.DecimalField(max_digits=9, decimal_places=6)
    address = models.TextField(unique=True)
    city = models.TextField()
    zipcode = models.TextField()
    blacklisted = models.BooleanField(default=False)

    creation_time = models.DateTimeField(default=timezone.now)

    patrons = models.ManyToManyField(User)

    def thumbs_down(self):
        """
        Users can thumbs down a place. It will not be considered in the future.
        """
        self.blacklisted = True
        self.save()

    @property
    def past_visitors(self):
        return self.patrons.all()

    @property
    def reviews(self):
        return self.review_set.all()

    def add_review(self, user, text):
        self.review_set.create(text=text, created_by=user)

    def __unicode__(self):
        return self.name


class Review(models.Model):
    text = models.TextField()
    place = models.ForeignKey(Place)
    created_by = models.ForeignKey(User)

    creation_time = models.DateTimeField(default=timezone.now)

    @property
    def comments(self):
        return self.comment_set.all()

    def add_comment(self, text):
        return self.comment_set.create(text=text)

    def __unicode__(self):
        text = self.text[:10]                       # truncated
        return "{} - {}".format(self.place, text)


class Comment(models.Model):
    text = models.TextField()
    review = models.ForeignKey(Review)

    creation_time = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        text = self.text[:10]
        return "{} - {}".format(text, self.review)


class UserProfile(models.Model):

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other')
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default='M')
    address = models.TextField(unique=True)
    city = models.TextField()
    zipcode = models.TextField()
    avatar = models.ImageField(upload_to="avatars/", blank=True)
    user = models.OneToOneField(User)

    def __unicode__(self):
        return self.user.username
