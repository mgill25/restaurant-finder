__author__ = 'manish'

# Custom tags and filters here

from django import template

register = template.Library()

@register.filter
def capitalize(value):
    """Capitalize the given string"""
    return value.capitalize()

