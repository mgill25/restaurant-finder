__author__ = 'manish'

from django import forms
from django.utils.translation import ugettext as _

from .models import (
    User,
    Place,
    Review,
    Comment,
    UserProfile
)


class LoginForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['username', 'password']
        widgets = {
            'password': forms.PasswordInput()
        }


class SignupForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']
        widgets = {
            'password': forms.PasswordInput()
        }


class ReviewForm(forms.ModelForm):

    class Meta:
        model = Review
        fields = ['text']
        widgets = {
            'text': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'rows': 3,
                    'placeholder': 'Add a new review...'
                }
            )
        }


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'New comment...'
                }
            )
        }

# Reference not working inside class, so making it global here.
place_widget_input = forms.TextInput(
    attrs={
        'class': 'form-control'
    }
)


class PlaceForm(forms.ModelForm):

    class Meta:
        model = Place
        exclude = ['blacklisted', 'patrons', 'creation_time', 'lat', 'lon']
        widgets = dict(
            (k, place_widget_input) for k in
            ['name', 'address', 'city', 'zipcode'],
        )

GENDER_CHOICES = (
    (1, _("Male")),
    (2, _("Female")),
    (3, _("Other"))
)


class UserProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        fields = ['gender', 'address', 'city', 'zipcode']

        widgets = {
            'address': forms.TextInput(attrs={ 'class': 'form-control'}),
            'city': forms.TextInput(attrs={ 'class': 'form-control'}),
            'zipcode': forms.TextInput(attrs={ 'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'})
        }
